<?php

class Human implements SplSubject {

    private $name;
    private $mate;
    private $phone;
    public $bosses = array();

    public function __construct($name, $phone) {
        $this->name = $name;
        $this->phone = $phone;
        echo 'hello! I am ' . $this->name . '. My phone is ' . $this->phone . "<br>\n";
    }

    public function wedding(Human $mate) {

        if (null !== ($mate->getMate()) || null !== ($this->getMate())) {
            echo "Wedding is imposible<br>\n";
            return false;
        }
        $this->mate = $mate;
        $mate->mate = $this;
        echo $this->getName() . ' & ' . $mate->getName() . " just married!<br>\n";
        return true;
    }

    public function getName() {
        return $this->name;
    }

    public function getMate() {
        return $this->mate;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($newPhone) {
        $this->phone = $newPhone;
        $this->notify();
    }

    public function attach(SplObserver $observer) {
        return true;
    }

    public function detach(SplObserver $observer) {
        return true;
    }

    public function notify() {
        if ($this->getMate()->bosses !== null) {
            foreach ($this->getMate()->bosses as $boss) {
                $boss->update($this);
                return true;
            }
        }
        return false;
    }

}
