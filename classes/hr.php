<?php

class Hr implements SplObserver {

    private $employee = array();

    public function hire(Human $human) {
        foreach ($this->employee as $worker) {
            if ($worker === $human) {
                echo $human->getName() . " is alredy hired <br>\n";
                return false;
            }
        }
        $this->employee[] = $human;
        $human->bosses[] = $this;
        return true;
    }

    public function getEmployee() {
        foreach ($this->employee as $human) {
            $list[] = $human;
        }
        return $list;
    }

    public function update(SplSubject $subject) {
        echo $subject->getMate()->getName() 
                . ':' . $subject->getPhone()
                ."<br>\n";
        return true;
    }

}
